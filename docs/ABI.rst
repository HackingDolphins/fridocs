##############
Available ABIs
##############

.. index:: ABI
.. _ABI:

* default
* Windows 32-bit:
  + sysv
  + stdcall
  + thiscall
  + fastcall
  + mscdecl

* Windows 64-bit:
  + win64

* UNIX x86:
  + sysv
  + unix64

* UNIX ARM:
  + sysv
  + vfp
