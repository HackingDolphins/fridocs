#############################
Getting *Process* information
#############################

_________________
Basic information
_________________

The `Process` module gives us all the information about the process, such as imported modules, memory ranges, etc.

Let's see some of the functions provided to get familiar with Frida

.. code-block:: JavaScript
   :linenos:

    [Local::ls]-> Process.id
    10831
    [Local::ls]-> Process.arch
    "x64"
    [Local::ls]-> Process.platform
    "linux"

   
________________________
Getting imported modules
________________________

One of the most essential features we may want is to enumerate what is being imported and where it is located

.. code-block:: JavaScript
   :linenos:

    [Local::ls]-> Process.enumerateModulesSync()
    [
        {
            "base": "0x55b1a5a93000",
            "name": "ls",
            "path": "/bin/ls",
            "size": 2232320
        },
        {
            "base": "0x7ffefc7eb000",
            "name": "linux-vdso.so.1",
            "path": "linux-vdso.so.1",
            "size": 0
        },
        {
            "base": "0x7f5a6c51c000",
            "name": "libselinux.so.1",
            "path": "/lib/x86_64-linux-gnu/libselinux.so.1",
            "size": 2252800
        },
        ...
    ]


As we can see the result is shown in a convenient json format. This is particularly useful as we are in a JavaScript context, therefore json is an object and we can access its properties directly:

.. code-block:: JavaScript
   :linenos:

    [Local::ls]-> var modules = Process.enumerateModulesSync();
    undefined
    [Local::ls]-> console.log('The module \'' + modules[0].name + '\' is located at ' + modules[0].base)
    The module 'ls' is located at 0x55b1a5a93000


__________________
Get memory regions
__________________

Lets say we are inspecting a process and we want to know all the memory regions that have read, write and execution permissions enabled

.. code-block:: JavaScript
   :linenos:

    [Local::PID::11740]-> Process.enumerateRangesSync({protection:'rwx', coalesce:true})
    [
        {
            "base": "0x7fbc52940000",
            "file": {
                "offset": 253952,
                "path": "/lib/x86_64-linux-gnu/libc-2.27.so",
                "size": 0
            },
            "protection": "rwx",
            "size": 12288
        },
        {
            "base": "0x7fbc52945000",
            "file": {
                "offset": 274432,
                "path": "/lib/x86_64-linux-gnu/libc-2.27.so",
                "size": 0
            },
            "protection": "rwx",
            "size": 4096
        },
        {
            "base": "0x7fbc529e6000",
            "file": {
                "offset": 933888,
                "path": "/lib/x86_64-linux-gnu/libc-2.27.so",
                "size": 0
            },
            "protection": "rwx",
            "size": 4096
        }
    ]

The `coalesce:true` parameter joins all contiguous memory regions with same permissions in a single entry on the returned object. Disabling that option we'll get an entry for each memory page with the selected permissions


We can use the character '-' as a wildcard permission, f.e. if we want to search all executable memory regions but we don't care about it being readable or writable we can use `Process.enumerateRangesSync({protection:'--x', coalesce:true})`


