TODO:

* [✓] Parte 1:

  * [✓] Como inyectar frida
  * [✓] Leer, escribir, buscar patrones, etc en memoria (Memory.xxxx)
  * [✓] Como hookear funciones
  * [✓] Obtener el resultado de una funcion
  * [✓] NativeCallback? Se esta usando pero no se explica que hace ni como funciona
  * [✓] NativeFunction/SystemFunction? Idem

* [✗] Parte 2:
    
  * [✗] Explicar los modulos faltantes

    * [✓] Stalker

      * Falta explicar ``Stalker.addCallProbe`` pero no esta funcionando correctamente

    * [✗] Thread
    * [✗] RPC
    * [✗] Socket interface/Stream interface
    * [✗] Sqlite interfac? Vale la pena?
    * [✗] Instruction? Vale la pena?

    
* [✗] Parte 3:

  * [✗] Cosas especificas de cada plataforma
  * [✗] Kernel
  * [✗] Java
  * [✗] ObjC
  * [✗] Architectures (x86/x64, Arm, Arm64, MIPS, AArch, etc) API (Writer, Relocator, enum)

* [✗] Parte 4:

  * [✗] Integracion con radare2
  * [✗] Como funciona frida (Explicar DuckTape/V8, diferencias, etc)
  * [✗] Compilando nuestra propia DLL de frida
  * [✗] Gum internals


* [✗] Tesis:

  * [✗] Tetris en frida hookeando system32
