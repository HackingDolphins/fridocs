###############
Getting started
###############

______________________________
Getting on the road with Frida
______________________________

Frida can be used using bindings on different languages. Python is the most used, but there are bindings for .Net, Node.js and Java supported officially. Also there is the frida-core project that provides the libraries to create bindings for other languages.

The easiest way to get introduced into Frida is using its REPL console that uses the JavaScript API.

First of all we need to learn how to start an application with frida. Once we have :ref:`installed frida-tools <installation>` we'll try to open a binary file (`/bin/ls`) with frida attached.

.. code-block:: bash
   :emphasize-lines: 1

     # frida /bin/ls
       ____
      / _  |   Frida 12.2.22 - A world-class dynamic instrumentation toolkit
     | (_| |
      > _  |   Commands:
     /_/ |_|       help      -> Displays the help system
     . . . .       object?   -> Display information about 'object'
     . . . .       exit/quit -> Exit
     . . . .
     . . . .   More info at http://www.frida.re/docs/home/
     Spawned `/bin/ls`. Use %resume to let the main thread start executing!  

We can also inject frida on a running process. To do that we need to know the PID of the process we want to attach to

.. code-block:: bash

   # frida -p <PID>


_____________________
Other useful commands
_____________________

Connecting to a remote `frida-server` instance - f.e. rooted iOS/Android devices.

* USB connected device

.. code-block:: bash
   
   # frida -U [-D <Device ID>]

The device ID is an optional value in case we have multiple devices connected

* Through the network

.. code-block:: bash
   
   # frida -R -H <Host or IP>


_______
Modules
_______

Frida is a big framework that allows us to dynamically instrumentate a wide variety of process layouts including different architectures or virtual machine - like the Dalvik JVM used in Android. In order to do that Frida provides some abstractions that will help us to interact with the Frida core injected in the process.

These helper abstractions are organized in different modules that server different purpouses:

* :doc:`Process <process>`
      Gives information about the process (PID, architecure it runs on, imported modules, running threads, etc)

* :doc:`Memory <memory>`
      Manages interactions with the process memory, from read/write to allocation and permissions.

* :doc:`Module <module>`
      Find functions in the process or its imports

* :doc:`NativeFunction <NativeFunction>`
      Get functions available within the process to use it from JavaScriipt

* :ref:`NativeCallback <NativeCallback>`
      Get functions available within the process to use it from JavaScriipt
* :doc:`Interceptor <interceptor>`
      Hook and replace functions, retrieve return values, etc
