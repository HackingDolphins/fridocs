############
Introduction
############

______________
What is Frida?
______________

Frida is a dynamic instrumentation library designed to work on multiple platforms and architectures such as Android, iOS, Windows, Linux, etc.

It serves multiple purpouses such as:

* Blackbox testing of precompiled binaries
* Inspect a process memory, imports and exports
* Dynamically modify the process behaviour (Function hooking or override)
* Add new behaviour to the application
* Inspect method calls and view or modify its parameters
* Have fun!

All of this can be done using JavaScript, but bindings for multiple languages like Python, C, Swift, .Net, etc are available.



.. index:: Installation
.. _installation:

____________
Installation
____________

Usually, you would want to install `frida-tools` to have a CLI interface for fast prototyping and other useful tools:

    `pip install frida-tools`

Also, you can install just the `frida` core [#]_ and use it directly from a python script. This is particularly useful when developing new tools, snippets or entire application that work on top of `frida`

    `pip install frida`


.. rubric:: footnotes
..    [#] Note: frida core is already included as a dependency of `frida-tools`, there is no need to install it twice
