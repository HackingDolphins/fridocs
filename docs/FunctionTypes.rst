##############
Function types
##############

.. index:: FunctionTypes
.. _FunctionTypes: 

* void
* pointer
* int
* uint
* long
* ulong
* char
* uchar
* float
* double
* int8
* uint8
* int16
* uint16
* int32
* uint32
* int64
* uint64
* bool

For functions that receive or returns structs, just use an array with the types it contains
