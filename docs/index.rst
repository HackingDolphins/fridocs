.. FriDocs documentation master file, created by
   sphinx-quickstart on Fri Feb 22 13:55:48 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to FriDocs's documentation!
===================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   introduction.rst
   getting_started.rst
   process.rst
   memory.rst
   module.rst
   NativeFunction.rst
   interceptor.rst
   netcat_interceptor.rst
   stalker.rst




Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
