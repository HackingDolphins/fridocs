# FriDocs

## What is?

FriDocs is a Frida documentation and tutorial

## How to use

FriDocs is built on top of Sphinx, you can read it accessing directly the \*.rst files. But if you want to have all the features Sphinx has such as searching content, inter-document links and other nice QoL features you can clone this repository and navigate the html version under `docs/.build/html/index.html` or compile it yourself in other formats `make`ing the docs using the `docs/Makefile`

Some interesting output formats:
* HTML (Prefered one and pre-compiled)
* Markdown
* EPUB

## How can I do **XXXX**?

If you think there is some feature or example missing, please create an issue explaining what you are trying to achieve so I can add it in the future.

Or you can do it yourself and create a pull request. There is no better way to learn something than doing it yourself!
